<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit176a07bbb83ec24feeb7280bfe253807
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit176a07bbb83ec24feeb7280bfe253807::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit176a07bbb83ec24feeb7280bfe253807::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
